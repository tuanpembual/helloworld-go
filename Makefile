VERSION:=$(shell cat VERSION)

LDFLAGS="-X main.appVersion=$(VERSION)"

all:
	CGO_ENABLED=0 go build -ldflags=$(LDFLAGS) -o main --installsuffix cgo main.go
	docker build -t tuanpembual/helloworld:$(VERSION) .