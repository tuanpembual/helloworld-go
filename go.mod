module github.com/tuanpembual/helloworld

go 1.18

require (
  github.com/prometheus/client_golang v1.3.0
  golang.org/x/net v0.0.0-20190613194153-d28f0bde5980
)

replace github.com/prometheus/client_golang => github.com/prometheus/client_golang v1.3.0
