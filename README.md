# README

base on https://github.com/brancz/prometheus-example-app

```
export GOPATH=$GOROOT
unset GOROOT
make all
```

# Deploy

```
kubectl create ns helloworld
kubectl apply -f deployment.yaml
```